const $ = (selector) =>{
    return document.querySelector(selector)
}
const hour = $('.hour');
const dot = $('.dot');
const min = $('.min');
const weeks = $('.weeks');
let showDot = true;

function update(){
    showDot = !showDot;
    const now = new Date();

    if (showDot) {
        dot.classList.add('invisible');
    }else {
        dot.classList.remove('invisible');
    }
    hour.textContent = String(now.getHours()).padStart(2, '0');
    min.textContent = String(now.getMinutes()).padStart(2, '0');

    Array
        .from(weeks.children)
        .forEach(
            (ele) =>{
                ele.classList.remove('active')
    }
        );
    weeks
        .children[now.getDay()]
        .classList
        .add('active');
};

setInterval(update, 500);