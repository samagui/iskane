import os

from django.contrib.auth.models import AbstractUser
from django.db import models


# Create your models here.

def upload_to(instance, filename):
    path = f'users/{instance.username}'
    extension = filename.split('.')[-1]
    if extension:
        path = path + '.' + extension
    return path


class User(AbstractUser):
    email = models.EmailField(unique=True)
    telephone = models.CharField(max_length=11, blank=True, null=True)
    profile_pic = models.ImageField(default='default/user.png', upload_to=upload_to, verbose_name="Photo")

    def __str__(self):
        return self.username
