from django.urls import path

from auth_app.views import *

app_name = 'auth'
urlpatterns = [
    path('signin/', signin, name='signin'),
    path('logout/', custom_logout, name='logout'),
    path('signup/', signup, name='signup'),
    path('profile/', profile, name='profile'),
    path('lock/', lock, name='lock'),
]
