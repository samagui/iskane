from django.contrib import messages
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from auth_app.forms import UserRegistrationForm, UserLoginForm, UserUpdateForm, ChangePasswordForm


# Create your views here.

def signin(request):
    if request.method == "POST":
        form = UserLoginForm(request=request, data=request.POST)
        if form.is_valid():
            user = authenticate(
                username=form.cleaned_data["username"],
                password=form.cleaned_data["password"],
            )
            if user is not None:
                login(request, user)
                messages.success(request, f"Bievenue {user.username}! Vous êtes maintenant connecté!")
                return redirect("index")
    form = UserLoginForm()

    return render(request, 'users/signin.html', context={"form": form})


@login_required
def custom_logout(request):
    logout(request)
    messages.info(request, "Logged out successfully!")
    return redirect("auth:signin")


def signup(request):
    if request.method == "POST":
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = True
            user.save()
            # activateEmail(request, user, form.cleaned_data.get('email'))
            return redirect('auth:signin')

        else:
            for error in list(form.errors.values()):
                messages.error(request, error)
                print(error)

    else:
        form = UserRegistrationForm()

    return render(request, 'users/signup.html', context={"form": form})

def lock(request):
    user = request.user
    username = user.username

    return render(request, 'users/lock.html')


def profile(request):
    user = request.user
    user_form = UserUpdateForm(instance=user)
    change_password_form = ChangePasswordForm(user=user)
    context = {
        'user_form': user_form,
        'change_password_form': change_password_form
    }
    return render(request, 'users/profile.html', context)

def profile_edit(request):
    data = dict()
    user = request.user
    if request.method == "POST":
        form = UserUpdateForm(request.POST, user=user)
        if form.is_valid():
            _user = form.save()
            data['form_is_valid'] = False
            data['message'] = "Informations mis à jour avec succès"
        else:
            data['form_is_valid'] = False



@login_required()
def add_active_grade(request, id):
    data = dict()
    enrollment = Enrollment.objects.get(id=id)
    if request.method == "POST":
        form = GradeForm(request.POST, enrollment=enrollment)
        if form.is_valid():
            grade = form.save(commit=False)
            try:
                check_grade = Grade.objects.get(enrollment=grade.enrollment, semester=grade.semester)
                message = messages.error(request, "Ce résultat existe déjà dans la base de données")
                data['form_is_valid'] = False
                data['message'] = message
            except ObjectDoesNotExist:
                grade.enrollment = enrollment
                grade.save()
                messages.success(request, "Résultat enregistré avec succès")
                data['form_is_valid'] = True
                grades = Grade.objects.filter(active=True).order_by("-id")
                data['grade_list'] = render_to_string('app/academy/results/new/list.html',
                                                      {'grades': grades})
        else:
            data['form_is_valid'] = False
    else:
        form = GradeForm(enrollment=enrollment)
        context = {'form': form, 'enrollment': enrollment}
        data['html_form'] = render_to_string(
            'app/academy/results/new/create.html', context, request=request)
    return JsonResponse(data)

