from django.shortcuts import render, get_object_or_404
from app.models import Promotion
from app.forms import PromotionForm
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.contrib.auth.decorators import login_required
from django.contrib import messages


@login_required()
def list(request):
    promotions = Promotion.objects.all().order_by("label")
    context = {
        'promotions': promotions,
        'segment_': 'settings',
        'segment': 'promotion',
    }
    return render(request, 'app/settings/promotions/list.html', context)


@login_required()
def save_all(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            promotions = Promotion.objects.all().order_by("-date_end")
            messages.success(request, "Année enregistrée avec succès")
            message = messages.success(request, "Année enregistrée avec succès")
            data['message'] = message
            data['promo_list'] = render_to_string('app/settings/promotions/list.html', {'promotions': promotions})
        else:
            data['form_is_valid'] = False
    context = {
        'form': form
    }
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)

@login_required()
def create(request):
    if request.method == 'POST':
        form = PromotionForm(request.POST)
    else:
        form = PromotionForm()
    return save_all(request, form, 'app/settings/promotions/create.html')


@login_required()
def update(request, id):
    promotion = get_object_or_404(Promotion, id=id)
    if request.method == 'POST':
        form = PromotionForm(request.POST, instance=promotion)
    else:
        form = PromotionForm(instance=promotion)
    return save_all(request, form, 'app/settings/promotions/update.html')
