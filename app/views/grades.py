from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.template.loader import render_to_string

from app.forms import GradeForm
from app.models import Grade, Enrollment


@login_required()
def newGrades(request):
    grades = Grade.objects.filter(enrollment__student__active=True).order_by('enrollment__student__matricule')
    context = {'grades': grades}
    return render(request, 'app/academy/results/active-grades.html', context)


@login_required()
def oldGrades(request):
    grades = Grade.objects.filter(enrollment__student__active=False).order_by('enrollment__student__matricule')
    context = {'grades': grades}
    return render(request, 'app/academy/results/old-grades.html', context)


@login_required()
def add_active_grade(request, id):
    data = dict()
    enrollment = Enrollment.objects.get(id=id)
    if request.method == "POST":
        form = GradeForm(request.POST, enrollment=enrollment)
        if form.is_valid():
            grade = form.save(commit=False)
            try:
                check_grade = Grade.objects.get(enrollment=grade.enrollment, semester=grade.semester)
                message = messages.error(request, "Ce résultat existe déjà dans la base de données")
                data['form_is_valid'] = False
                data['message'] = message
            except ObjectDoesNotExist:
                grade.enrollment = enrollment
                grade.save()
                messages.success(request, "Résultat enregistré avec succès")
                data['form_is_valid'] = True
                grades = Grade.objects.filter(active=True).order_by("-id")
                data['grade_list'] = render_to_string('app/academy/results/new/list.html',
                                                      {'grades': grades})
        else:
            data['form_is_valid'] = False
    else:
        form = GradeForm(enrollment=enrollment)
        context = {'form': form, 'enrollment': enrollment}
        data['html_form'] = render_to_string(
            'app/academy/results/new/create.html', context, request=request)
    return JsonResponse(data)
