import json

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, FormView

from app.admin import StudentResource, OldStudentResource, ActiveStudentResource
from app.forms import StudentForm, OldStudentForm, FormatForm
from app.models import Student, Enrollment, AnnualAverage


def gen_matritule():
    students = Student.objects.all()
    if students.exists():
        student = students.latest('id')
        last_matricule = student.matricule
        num = int(last_matricule[-6:]) + 1
        if num < 10:
            matricule = "ISBJ" + "00000" + str(num)
        elif num < 100:
            matricule = "ISBJ" + "0000" + str(num)
        elif num < 1000:
            matricule = "ISBJ" + "000" + str(num)
        elif num < 10000:
            matricule = "ISBJ" + "00" + str(num)
        elif num < 100000:
            matricule = "ISBJ" + "0" + str(num)
        else:
            matricule = "ISBJ" + str(num)
    else:
        matricule = "ISBJ000001"

    return matricule


# @login_required()
class StudentsListView(ListView, FormView):
    model = Student
    template_name = 'app/students/list.html'
    form_class = FormatForm

    def post(self, request, **kwargs):
        qs = self.get_queryset()
        dataset = StudentResource().export(qs)

        format = request.POST.get('format')
        if format == 'xls':
            ds = dataset.xls
        elif format == 'csv':
            ds = dataset.csv
        elif format == 'json':
            ds = dataset.json
        elif format == 'xlsx':
            ds = dataset.xlsx
        response = HttpResponse(ds, content_type=f'{format}')
        response['content-disposition'] = f'attachement; filename="pensionnaires.{format}'
        return response


# @login_required()
class OldStudentsListView(ListView, FormView):
    model = Student
    queryset = Student.objects.filter(active=False)
    template_name = 'app/students/old.html'
    form_class = FormatForm

    def post(self, request, **kwargs):
        qs = Student.objects.filter(active=False)
        dataset = OldStudentResource().export(qs)

        format = request.POST.get('format')
        if format == 'xls':
            ds = dataset.xls
        elif format == 'csv':
            ds = dataset.csv
        elif format == 'json':
            ds = dataset.json
        elif format == 'xlsx':
            ds = dataset.xlsx
        response = HttpResponse(ds, content_type=f'{format}')
        response['content-disposition'] = f'attachement; filename="anciens.{format}'
        return response


# @login_required()
class ActiveStudentsListView(ListView, FormView):
    model = Student
    queryset = Student.objects.filter(active=True)
    template_name = 'app/students/actifs.html'
    form_class = FormatForm

    def post(self, request, **kwargs):
        qs = Student.objects.filter(active=True)
        dataset = ActiveStudentResource().export(qs)

        format = request.POST.get('format')
        if format == 'xls':
            ds = dataset.xls
        elif format == 'csv':
            ds = dataset.csv
        elif format == 'json':
            ds = dataset.json
        elif format == 'xlsx':
            ds = dataset.xlsx
        response = HttpResponse(ds, content_type=f'{format}')
        response['content-disposition'] = f'attachement; filename="actifs.{format}'
        return response


@login_required()
def save_new(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        form = StudentForm(request)
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            students = Student.objects.filter(active=True)
            data['branch_list'] = render_to_string('app/students/actifs.html', {'students': students})
        else:
            data['form_is_valid'] = False
    context = {
        'form': form
    }
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@login_required()
def add_new(request):
    matricule = gen_matritule()
    if request.method == 'POST':
        form = StudentForm(request.POST, request.FILES)
        if form.is_valid():
            try:
                student = form.save(False)
                student.matricule = matricule
                student.active = True
                student.save()
                messages.success(request, "Pensionnaire enregistrée avec succès")
                return redirect("add-new-student")
            except Exception as e:
                messages.error(request, "Enregistrement échoué")
                return redirect("add-new-student")
        else:
            # print(form.errors)
            # messages.error(request, "Enregistrement échoué")
            for error in list(form.errors.values()):
                messages.error(request, error)
            return redirect("add-new-student")
    # else:
    #     form = StudentForm()
    form = StudentForm()

    segment_ = "pensionnaire"
    segment = "add-new"
    context = {
        'segment_': segment_,
        'segment': segment,
        'form': form,
        'matricule': matricule,
    }
    return render(request, 'app/students/add-new.html', context)


@login_required()
def update_student(request, id):
    student = get_object_or_404(Student, id=id)
    if request.method == 'POST':
        form = OldStudentForm(request.POST, request.FILES, instance=student)
        if form.is_valid():
            form.save()
            messages.success(request, "Modifications effectuées avec succès")
            return redirect('student-actives')
    else:
        form = OldStudentForm(instance=student)

    context = {
        'form': form,
        'matricule': student.matricule,
        'instance': student
    }
    return render(request, 'app/students/add-old.html', context)


@login_required()
def detail_student(request, id):
    student = get_object_or_404(Student, id=id)
    enrollments = Enrollment.objects.filter(student=student)
    results = AnnualAverage.objects.filter(student=student)
    print(results)
    context = {
        'student': student,
        'enrollments': enrollments,
        'results': results,
        'p': 75
    }
    return render(request, 'app/students/details.html', context)


@login_required()
def add_student(request):
    form = StudentForm()
    students = Student.objects.all()
    if students.exists():
        student = students.latest('id')
        last_matricule = student.matricule
        num = int(last_matricule[-6:]) + 1
        if num < 10:
            matricule = "ISBJ" + "00000" + str(num)
        elif num < 100:
            matricule = "ISBJ" + "0000" + str(num)
        elif num < 1000:
            matricule = "ISBJ" + "000" + str(num)
        elif num < 10000:
            matricule = "ISBJ" + "00" + str(num)
        elif num < 100000:
            matricule = "ISBJ" + "0" + str(num)
        else:
            matricule = "ISBJ" + str(num)
    else:
        matricule = "ISBJ000001"

    segment_ = "pensionnaire"
    segment = "add"
    context = {
        'segment_': segment_,
        'segment': segment,
        # 'datas': pens,
        'form': form,
        'matricule': matricule,
    }
    if request.method == 'POST':
        # etudiant_form = EtudiantForm(request.POST, request.FILES)
        form = StudentForm(request.POST, request.FILES)
        if form.is_valid():
            stdt = form.save(False)
            stdt.matricule = matricule
            stdt.save()
            messages.success(request, "Pensionnaire enregistrée avec succès")
            return redirect("add-new-student")
            # return render(request, 'pensionnaire/add.html', context)
        else:
            print(form.errors)
            messages.error(request, "Enregistrement échoué")
            for error in list(form.errors.values()):
                messages.error(request, error)
            return redirect("add-new-student")
    return render(request, 'app/students/add-new.html', context)


@login_required()
def add_old(request):
    form = OldStudentForm()
    matricule = gen_matritule()
    segment_ = "pensionnaire"
    segment = "add"
    context = {
        'segment_': segment_,
        'segment': segment,
        'form': form,
        'matricule': matricule,
    }
    if request.method == 'POST':
        form = OldStudentForm(request.POST, request.FILES)
        if form.is_valid():
            stdt = form.save(False)
            stdt.matricule = matricule
            stdt.active = False
            stdt.save()
            messages.success(request, "Pensionnaire enregistrée avec succès")
            return redirect("add-old-student")
            # return render(request, 'pensionnaire/add.html', context)
        else:
            print(form.errors)
            messages.error(request, "Enregistrement échoué")
            for error in list(form.errors.values()):
                messages.error(request, error)
            return redirect("add-old-student")
    return render(request, 'app/students/add-old.html', context)
