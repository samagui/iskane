from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt

from app.forms import EnrollmentForm, OldEnrollmentForm, GradeForm
from app.models import Enrollment, Grade


@login_required()
def newEnrollments(request):
    enrollments = Enrollment.objects.filter(active=True, closed=False)
    form = GradeForm()
    context = {'enrollments': enrollments, 'form': form}
    return render(request, 'app/academy/enrollment/enrollments.html', context)


@login_required()
def oldEnrollments(request):
    enrollments = Enrollment.objects.filter(active=False, closed=False).order_by("-id")
    form = GradeForm()
    context = {'enrollments': enrollments, 'form': form}
    return render(request, 'app/academy/enrollment/old-enrollments.html', context)


@login_required()
def add_active(request):
    data = dict()
    form = EnrollmentForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            enrollment = form.save(commit=False)
            try:
                check_enrollment = Enrollment.objects.get(student=enrollment.student,
                                                          academicYear=enrollment.academicYear,
                                                          year=enrollment.year)
                message = "Cette inscription existe déjà dans la base de données"
                data['form_is_valid'] = False
                data['message'] = message
            except ObjectDoesNotExist:
                enrollment.active = True
                enrollment.closed = False
                enrollment.save()
                message = "Inscription enregistrée avec succès"
                data['form_is_valid'] = True
                data['message'] = message
                enrollments = Enrollment.objects.filter(active=True, closed=False).order_by("-id")
                data['enrollment_list'] = render_to_string('app/academy/enrollment/new_enr_list2.html',
                                                           {'enrollments': enrollments})
        else:
            data['form_is_valid'] = False
    else:
        context = {'form': form}
        data['html_form'] = render_to_string(
            'app/academy/enrollment/add-new.html', context, request=request)
    return JsonResponse(data)


@login_required()
def update_active(request, id):
    data = dict()
    enrollment = get_object_or_404(Enrollment, id=id)
    if request.method == "POST":
        form = EnrollmentForm(request.POST, instance=enrollment)
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            enrollments = Enrollment.objects.filter(active=True, closed=False, student__active=True).order_by("-id")
            message = "Modifications enregistrées avec succès"
            data['message'] = message
            data['enrollment_list'] = render_to_string('app/academy/enrollment/new_enr_list2.html',
                                                       {'enrollments': enrollments})
        else:
            data['form_is_valid'] = False
            message = "Modifications échouées"
            data['message'] = message
    else:
        form = EnrollmentForm(instance=enrollment)
        context = {'form': form}
        data['html_form'] = render_to_string(
            'app/academy/enrollment/update-new.html', context, request=request)
    return JsonResponse(data)


@login_required()
def add_old(request):
    data = dict()
    if request.method == "POST":
        form = OldEnrollmentForm(request.POST)
        if form.is_valid():
            ancien = form.save(commit=False)  # Utilisez commit=False pour ne pas sauvegarder immédiatement
            try:
                check_enrollment = Enrollment.objects.get(student=ancien.student, academicYear=ancien.academicYear,
                                                          year=ancien.year)
                message = "Cette inscription existe déjà dans la base de données"
                data['form_is_valid'] = False
                data['message'] = message
            except ObjectDoesNotExist:
                ancien.active = False
                ancien.closed = False
                ancien.save()
                data['message'] = "Inscription enregistrée avec succès"
                data['form_is_valid'] = True
                enrollments = Enrollment.objects.filter(active=False, closed=False).order_by("-id")
                data['enrollment_list'] = render_to_string('app/academy/enrollment/old_enr_list2.html',
                                                           {'enrollments': enrollments})
        else:
            data['form_is_valid'] = False
            message = "Inscriptions non enregistrée"
            data['message'] = message
    else:
        form = OldEnrollmentForm()
    context = {'form': form}
    data['html_form'] = render_to_string('app/academy/enrollment/add-old.html', context, request=request)
    return JsonResponse(data)


@login_required()
def update_old(request, id):
    data = dict()
    enrollment = get_object_or_404(Enrollment, id=id)
    if request.method == "POST":
        form = OldEnrollmentForm(request.POST, instance=enrollment)
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            enrollments = Enrollment.objects.filter(active=False, closed=False).order_by("-id")
            data['message'] = "Modifications enregistrées avec succès"
            data['enrollment_list'] = render_to_string('app/academy/enrollment/old_enr_list2.html',
                                                       {'enrollments': enrollments})
        else:
            data['form_is_valid'] = False
            data['message'] = "Modifications échouées"
    else:
        form = OldEnrollmentForm(instance=enrollment)
        context = {'form': form}
        data['html_form'] = render_to_string(
            'app/academy/enrollment/update-old.html', context, request=request)
    return JsonResponse(data)


@login_required()
@csrf_exempt
def save_grade(request):
    data = dict()
    if request.method == 'POST':
        enrollment_id = request.POST.get('enrollment_id')
        enrollment = get_object_or_404(Enrollment, id=enrollment_id)
        form = GradeForm(request.POST)
        if form.is_valid():
            grade = form.save(commit=False)
            try:
                check_grade = Grade.objects.get(enrollment=enrollment, semester=grade.semester)
                message = "Ce résultat existe déjà dans la base de données"
                data['form_is_valid'] = False
                data['message'] = message
            except ObjectDoesNotExist:
                # grade.enrollment = enrollment
                # grade = form.save(commit=False)
                grade.enrollment = enrollment
                grade.save()
                check_enrol_grades = Grade.objects.filter(enrollment=enrollment).count()
                if check_enrol_grades == 2:
                    enrollment.closed = True
                    enrollment.save()
                    enrollment.academicYear.calculate_annual_average()
                data['message'] = "Résultat enregistré avec succès"
                form = GradeForm()
                data['form_is_valid'] = True
                enrollments = Enrollment.objects.filter(active=True, closed=False).order_by("-id")
                data['enrollment_list'] = render_to_string('app/academy/enrollment/new_enr_list2.html',
                                                           {'enrollments': enrollments, 'form': form})
        else:
            data['form_is_valid'] = False
            data['message'] = "Enregistrement échoué"

    else:
        form = GradeForm()
        context = {'form': form}
        data['html_form'] = render_to_string(
            'app/academy/results/new/create.html', context, request=request)
    return JsonResponse(data)


@login_required()
@csrf_exempt
def save_grade_old(request):
    data = dict()
    if request.method == 'POST':
        enrollment_id = request.POST.get('enrollment_id')
        enrollment = get_object_or_404(Enrollment, id=enrollment_id)
        form = GradeForm(request.POST)
        if form.is_valid():
            grade = form.save(commit=False)
            try:
                check_grade = Grade.objects.get(enrollment=enrollment, semester=grade.semester)
                message = "Ce résultat existe déjà dans la base de données"
                data['form_is_valid'] = False
                data['message'] = message
            except ObjectDoesNotExist:
                # grade.enrollment = enrollment
                # grade = form.save(commit=False)
                grade.enrollment = enrollment
                grade.save()
                check_enrol_grades = Grade.objects.filter(enrollment=enrollment).count()
                if check_enrol_grades == 2:
                    enrollment.closed = True
                    enrollment.save()
                    enrollment.academicYear.calculate_annual_average()
                data['message'] = "Résultat enregistré avec succès"
                form = GradeForm()
                data['form_is_valid'] = True
                enrollments = Enrollment.objects.filter(active=False, closed=False).order_by("-id")
                data['enrollment_list'] = render_to_string('app/academy/enrollment/old_enr_list2.html',
                                                           {'enrollments': enrollments, 'form': form})
        else:
            data['form_is_valid'] = False
            data['message'] = "Enregistrement échoué"

    else:
        form = GradeForm()
        context = {'form': form}
        data['html_form'] = render_to_string(
            'app/academy/results/new/create.html', context, request=request)
    return JsonResponse(data)
