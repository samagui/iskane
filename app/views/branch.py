from django.shortcuts import render, get_object_or_404
from app.models import Branch
from app.forms import BranchForm
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.contrib.auth.decorators import login_required
from django.contrib import messages


@login_required()
def list(request):
    branches = Branch.objects.all()
    context = {
        'branches': branches,
        'segment_': 'settings',
        'segment': 'branch',
    }
    return render(request, 'app/settings/branches/list.html', context)


@login_required()
def save_all(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            branches = Branch.objects.all()
            data['branch_list'] = render_to_string('app/settings/branches/list.html', {'branches': branches})
        else:
            data['form_is_valid'] = False
    context = {
        'form': form
    }
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@login_required()
def create(request):
    if request.method == 'POST':
        form = BranchForm(request.POST)
    else:
        form = BranchForm()
    return save_all(request, form, 'app/settings/branches/create.html')


@login_required()
def update(request, id):
    branch = get_object_or_404(Branch, id=id)
    if request.method == 'POST':
        form = BranchForm(request.POST, instance=branch)
    else:
        form = BranchForm(instance=branch)
    return save_all(request, form, 'app/settings/branches/update.html')
