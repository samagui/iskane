from django.shortcuts import render, get_object_or_404
from app.models import AcademicYear
from app.forms import YearForm
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.contrib.auth.decorators import login_required
from django.contrib import messages


def list(request):
    annees = AcademicYear.objects.all().order_by("-end")
    context = {
        'years': annees,
        'segment_': 'settings',
        'segment': 'annee',
    }
    return render(request, 'app/settings/years/list.html', context)


def save_all(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            annees = AcademicYear.objects.all().order_by("-end")
            messages.success(request, "Année enregistrée avec succès")
            message = messages.success(request, "Année enregistrée avec succès")
            data['message'] = message
            data['annee_list'] = render_to_string('app/settings/years/list.html', {'years': annees})
        else:
            data['form_is_valid'] = False
    context = {
        'form': form
    }
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def create(request):
    if request.method == 'POST':
        form = YearForm(request.POST)
    else:
        form = YearForm()
    return save_all(request, form, 'app/settings/years/create.html')


def update(request, id):
    annee = get_object_or_404(AcademicYear, id=id)
    if request.method == 'POST':
        form = YearForm(request.POST, instance=annee)
    else:
        form = YearForm(instance=annee)
    return save_all(request, form, 'app/settings/years/update.html')


# def delete(request, id):
#     data = dict()
#     annee = get_object_or_404(AcademicYear, id=id)
#     if request.method == "POST":
#         annee.delete()
#         data['form_is_valid'] = True
#         annees = AcademicYear.objects.all().order_by("-id")
#         data['annee_list'] = render_to_string(
#             'settings/annee_list2.html', {'annee_datas': annees})
#     else:
#         context = {'annee': annee}
#         data['html_form'] = render_to_string(
#             'settings/annee_delete.html', context, request=request)
#
#     return JsonResponse(data)
