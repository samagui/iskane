from django.shortcuts import render, get_object_or_404
from app.models import School
from app.forms import SchoolForm
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.contrib.auth.decorators import login_required
from django.contrib import messages


@login_required()
def list(request):
    schools = School.objects.all().order_by("label")
    context = {
        'schools': schools,
        'segment_': 'settings',
        'segment': 'school',
    }
    return render(request, 'app/settings/schools/list.html', context)


@login_required()
def save_all(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            schools = School.objects.all().order_by("label")
            messages.success(request, "Année enregistrée avec succès")
            message = messages.success(request, "Année enregistrée avec succès")
            data['message'] = message
            data['school_list'] = render_to_string('app/settings/schools/list.html', {'schools': schools})
        else:
            data['form_is_valid'] = False
    context = {
        'form': form
    }
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)



@login_required()
def create(request):
    if request.method == 'POST':
        form = SchoolForm(request.POST)
    else:
        form = SchoolForm()
    return save_all(request, form, 'app/settings/schools/create.html')


@login_required()
def update(request, id):
    school = get_object_or_404(School, id=id)
    if request.method == 'POST':
        form = SchoolForm(request.POST, instance=school)
    else:
        form = SchoolForm(instance=school)
    return save_all(request, form, 'app/settings/schools/update.html')
