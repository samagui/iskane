from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from app.models import AcademicYear, School, Promotion, Branch


@login_required()
def settings(request):
    years = AcademicYear.objects.all().order_by("-end")
    schools = School.objects.all().order_by("label")
    promotions = Promotion.objects.all().order_by("-date_end")
    branches = Branch.objects.all()
    context = {
        'years': years,
        'schools': schools,
        'promotions': promotions,
        'branches': branches,
    }
    return render(request, 'app/settings/settings.html', context)

