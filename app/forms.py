from django import forms

from app.models import AcademicYear, Promotion, School, Branch, Student, Enrollment, Grade
from iskane import settings


class YearForm(forms.ModelForm):
    start = forms.DateField(
        input_formats=settings.DATE_INPUT_FORMATS,
        widget=forms.TextInput(
            attrs={'class': 'form-control datetimepicker flatpickr-input', 'placeholder': 'Select date..',
                   'type': 'date'}
        ),
        label="Date début"
    )
    end = forms.DateField(
        input_formats=settings.DATE_INPUT_FORMATS,
        widget=forms.TextInput(
            attrs={'class': 'form-control datetimepicker flatpickr-input', 'placeholder': 'Select date..',
                   'type': 'date'}
        ),
        label="Date fin")

    class Meta:
        model = AcademicYear
        fields = '__all__'


class PromotionForm(forms.ModelForm):
    date_start = forms.DateField(
        input_formats=settings.DATE_INPUT_FORMATS,
        widget=forms.TextInput(
            attrs={'class': 'form-control datetimepicker flatpickr-input', 'placeholder': 'Select date..',
                   'type': 'date'}
        ),
        label="Date début"
    )
    date_end = forms.DateField(
        input_formats=settings.DATE_INPUT_FORMATS,
        widget=forms.TextInput(
            attrs={'class': 'form-control datetimepicker flatpickr-input', 'placeholder': 'Select date..',
                   'autocomplete': 'off', 'type': 'date'}
        ),
        label="Date fin")
    name = forms.CharField(label="Nom de la promotion", widget=forms.TextInput())

    class Meta:
        model = Promotion
        fields = '__all__'


class SchoolForm(forms.ModelForm):
    class Meta:
        model = School
        fields = '__all__'


class BranchForm(forms.ModelForm):
    class Meta:
        model = Branch
        fields = '__all__'


class StudentForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = '__all__'
        exclude = ['matricule', 'active', 'date_left', 'left_cause', 'social_situation']


class OldStudentForm(forms.ModelForm):
    actif = forms.CheckboxInput(
        attrs={'class': 'form-check-input', 'id': 'flexSwitchCheckChecked', 'checked': 'unchecked'
               }
    )

    class Meta:
        model = Student
        fields = '__all__'
        exclude = ['matricule']


# class InscForm(forms.ModelForm):
#     date_insc = forms.DateField(
#         input_formats=settings.DATE_INPUT_FORMATS,
#         widget=forms.DateInput(
#             attrs={'class': 'form-control flatpickr-no-config', 'placeholder': 'Selectionner une date..',
#                    'autocomplete': 'off'}
#         ),
#         label="Date d'inscription"
#     )
#
#     class Meta:
#         model = Inscription
#         fields = '__all__'
#         exclude = ['moyenne', 'mention', 'closed']


# class MoyForm(forms.ModelForm):
#     # pensionnaire = forms.ModelChoiceField(
#     #     queryset=Inscription.objects.filter(closed=False).values('pensionnaire'),
#     #     required=True,
#     #     widget=forms.Select(attrs={'class': 'form-control', 'readonly': 'true'})
#     # )
#
#     class Meta:
#         model = Inscription
#         fields = ['pensionnaire', 'moyenne', 'mention']

class FormatForm(forms.Form):
    FORMAT_CHOICES = (
        ('xlsx', 'Excel'),
        ('csv', 'Csv'),
        ('json', 'json')
    )
    format = forms.ChoiceField(choices=FORMAT_CHOICES, widget=forms.Select(attrs={'class': 'form-select'}))


class EnrollmentForm(forms.ModelForm):
    student = forms.ModelChoiceField(queryset=Student.objects.filter(active=True),
                                     widget=forms.Select(attrs={'class': 'form-select'}), label='Pensionnaire')
    academicYear = forms.ModelChoiceField(queryset=AcademicYear.objects.all().order_by('-end'),
                                          widget=forms.Select(attrs={'class': 'form-select'}))

    class Meta:
        model = Enrollment
        fields = '__all__'
        exclude = ['active', 'closed']


class OldEnrollmentForm(forms.ModelForm):
    student = forms.ModelChoiceField(queryset=Student.objects.filter(active=False),
                                     widget=forms.Select(attrs={'class': 'form-select',
                                                                "id": 'organizerSingle', 'data-choices': "data-choices",
                                                                'data-options': '{"removeItemButton":true,"placeholder":true}'}))

    class Meta:
        model = Enrollment
        fields = '__all__'
        exclude = ['active', 'closed']


class GradeForm(forms.ModelForm):
    # def __init__(self, *args, enrollment=None, **kwargs):
    #     super(GradeForm, self).__init__(*args, **kwargs)
    #     self.enrollment = enrollment


    class Meta:
        model = Grade
        fields = '__all__'
        exclude = ['enrollment']
