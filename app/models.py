from django.db import models
from django.db.models import Sum
from django_countries.fields import CountryField


# Create your models here.

class School(models.Model):
    code = models.CharField(max_length=10, null=True, blank=True)
    label = models.CharField(max_length=150)
    address = models.CharField(max_length=150)
    phone_number = models.CharField(max_length=20, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)

    def __str__(self):
        return f"{self.label}"


class Branch(models.Model):
    class Meta:
        verbose_name_plural = "Filières"
        verbose_name = "Filière"

    code = models.CharField(max_length=10, null=True, blank=True)
    school = models.ForeignKey(School, on_delete=models.CASCADE)
    label = models.CharField(max_length=150)

    def __str__(self):
        return f"{self.label} - {self.school.label}"


class Promotion(models.Model):
    name = models.CharField(max_length=100)
    date_start = models.DateField()
    date_end = models.DateField()

    def __str__(self):
        return f"{self.name}"

    def get_periode(self):
        return f"{self.date_start.year} - {self.date_end.year}"


class Student(models.Model):
    class Meta:
        verbose_name = "Pensionnaire"
        verbose_name_plural = "Pensionnaires"

    def upload_to(instance, filename):
        path = f'students/{instance.matricule}'
        extension = filename.split('.')[-1]
        if extension:
            path = path + '.' + extension
        return path

    matricule = models.CharField(max_length=10, unique=True)
    last_name = models.CharField(max_length=150, verbose_name="Prénoms")
    first_name = models.CharField(max_length=150, verbose_name="Nom")
    birth_date = models.DateField(verbose_name="Date de naissance")
    lieu_naissance = models.CharField(max_length=50, verbose_name="Lieu de naissance")
    phone = models.CharField(max_length=20, verbose_name="Téléphone", unique=True)
    email = models.EmailField(verbose_name="Adresse mail", unique=True)
    photo = models.ImageField(default='default/pensionnaire.png', upload_to=upload_to)
    date_entering = models.DateField(verbose_name="Date d'entrée")
    date_left = models.DateField(verbose_name="Date de sortie", null=True, blank=True)
    left_cause = models.CharField(max_length=200, verbose_name="Motif de sortie", null=True, blank=True)
    tutor = models.CharField(max_length=255, verbose_name="Tuteur")
    tutor_contact = models.CharField(max_length=150)
    tutor_address = models.CharField(max_length=150)
    active = models.BooleanField(default=True)
    country = CountryField()
    city = models.CharField(max_length=150, null=True, blank=True, verbose_name="Ville d'origine")
    promotion = models.ForeignKey(Promotion, on_delete=models.CASCADE)
    branch = models.ForeignKey(Branch, on_delete=models.CASCADE, null=True, blank=True, verbose_name="Filière")
    social_situation = models.CharField(max_length=200, null=True, blank=True,
                                        verbose_name="Situation sociale actuelle")

    def __str__(self):
        return f"{self.last_name} {self.first_name}"

    def calculate_annual_average(self, an):
        enrollments = Enrollment.objects.filter(academicYear__end=an)
        grades_total = sum(enrollments)
        total_grades = sum(
            enrollment.grade_set.aggregate(models.Sum('average'))['average__sum'] for enrollment in enrollments)
        total_credits = sum(enrollment.grade_set.count() for enrollment in enrollments)
        average = total_grades / total_credits if total_credits else 0
        AnnualAverage.objects.create(student=self, annee=an, average=average)
        return average




class AcademicYear(models.Model):
    start = models.DateField()
    end = models.DateField()

    class Meta:
        verbose_name = "Année Académique"
        verbose_name_plural = "Années Academiques"

    def __str__(self):
        return f"{self.start.year} - {self.end.year}"


    def calculate_annual_average(self):
        enrollments = Enrollment.objects.filter(academicYear=self)
        total_grades = 0
        total_credits = 0

        for enrollment in enrollments:
            grades_sum = enrollment.resultats.aggregate(Sum('average'))['average__sum'] or 0
            grades_count = enrollment.resultats.count()
            total_grades += grades_sum
            total_credits += grades_count

        average = total_grades / total_credits if total_credits else 0
        proport = (average * 100) // 20
        AnnualAverage.objects.create(student=enrollment.student, academic_year=self, average=average, proportion=proport)
        return average


class Enrollment(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE, verbose_name='Pensionnaire',
                                related_name='inscriptions')
    academicYear = models.ForeignKey(AcademicYear, on_delete=models.CASCADE, verbose_name='Année Academique')
    school = models.ForeignKey(School, on_delete=models.CASCADE, verbose_name='Ecole')
    active = models.BooleanField(default=True, null=True, blank=True)
    closed = models.BooleanField(default=False, null=True, blank=True)
    year = models.PositiveSmallIntegerField(null=True, blank=True,
                                            choices=[(1, 'Première'), (2, 'Deuxième'), (3, 'Troisième')],
                                            verbose_name='Niveau')

    class Meta:
        verbose_name = "Inscription"
        verbose_name_plural = "Inscriptions"


class Grade(models.Model):
    enrollment = models.ForeignKey(Enrollment, on_delete=models.CASCADE, verbose_name="Inscription",
                                   related_name="resultats")
    semester = models.IntegerField(choices=[(1, 'Premier'), (2, 'Deuxième')], verbose_name="Semestre")
    average = models.FloatField(verbose_name="Moyenne")
    grade = models.CharField(verbose_name="Mention", max_length=50, choices=[
        ('Passable', 'Passable'), ('Assez-Bien', 'Assez-Bien'), ('Bien', 'Bien'), ('Très-Bien', 'Très-Bien'),
        ('Excellent', 'Excellent')
    ])

    class Meta:
        verbose_name = "Moyenne"
        verbose_name_plural = "Moyennes"


class AnnualAverage(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE, related_name="annual_average")
    academic_year = models.ForeignKey(AcademicYear, on_delete=models.CASCADE)
    proportion = models.IntegerField(null=True, default=0)
    average = models.FloatField()

# class Inscription(models.Model):
#     label_choices = (
#         ('first', '1ère année'),
#         ('second', '2e année'),
#         ('third', '3e année'),
#     )
#     mention_choices = (
#         ('Passable', 'Passable'),
#         ('Assez-Bien', 'Assez-Bien'),
#         ('Bien', 'Bien'),
#         ('Très-Bien', 'Très-Bien'),
#         ('Excellent', 'Excellent'),
#     )
#     annee = models.ForeignKey(AcademicYear, on_delete=models.CASCADE)
#     filiere = models.ForeignKey(Branch, on_delete=models.CASCADE)
#     student = models.ForeignKey(Student, on_delete=models.CASCADE, related_name='inscriptions')
#     libelle = models.CharField(max_length=50, choices=label_choices)
#     moyenne = models.FloatField(null=True, blank=True)
#     pourcentage = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(100)],
#                                       default=0, null=True, blank=True)
#     mention = models.CharField(max_length=20, null=True, blank=True, choices=mention_choices)
#     closed = models.BooleanField(default=False, null=True, blank=True)
#     date_insc = models.DateField(null=True, blank=True)
