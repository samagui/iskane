from django.contrib import admin
from app.models import *
from import_export import resources
from import_export.fields import Field


class StudentResource(resources.ModelResource):
    active = Field()
    branch = Field()
    promotion = Field()

    class Meta(object):
        model = Student
        fields = (
            'id', 'matricule', 'photo', 'first_name', 'last_name', 'birth_date', 'lieu_naissance', 'email', 'phone',
            'date_entering', 'date_left', 'left_cause', 'tutor', 'active', 'country', 'city,'
                                                                                          'promotion', 'branch',
            'social_situation')
        export_order = ('first_name', 'last_name', 'birth_date', 'social_situation')

    def dehydrate_active(self, obj):
        if obj.active:
            return f"Actif"
        else:
            return f"Ancien"

    def dehydrate_branch(self, obj):
        return f"{obj.branch.label}"

    def dehydrate_promotion(self, obj):
        return f"{obj.promotion.name}"


class OldStudentResource(resources.ModelResource):
    active = Field()
    branch = Field()
    promotion = Field()

    class Meta(object):
        model = Student
        fields = (
            'id', 'matricule', 'photo', 'first_name', 'last_name', 'birth_date', 'lieu_naissance', 'email', 'phone',
            'date_entering', 'date_left', 'left_cause', 'tutor', 'active', 'country', 'city,'
                                                                                          'promotion', 'branch',
            'social_situation')
        export_order = ('first_name', 'last_name', 'birth_date', 'social_situation')

    def dehydrate_active(self, obj):
        if obj.active:
            return f"Actif"
        else:
            return f"Ancien"

    def dehydrate_branch(self, obj):
        return f"{obj.branch.label}"

    def dehydrate_promotion(self, obj):
        return f"{obj.promotion.name}"


class ActiveStudentResource(resources.ModelResource):
    active = Field()
    branch = Field()
    promotion = Field()

    class Meta(object):
        model = Student
        fields = (
            'matricule', 'first_name', 'last_name', 'birth_date', 'lieu_naissance', 'email', 'phone',
            'date_entering', 'tutor', 'active', 'country', 'city', 'promotion', 'branch')
        export_order = ('first_name', 'last_name', 'birth_date', 'promotion', 'country', 'city')

    def dehydrate_active(self, obj):
        if obj.active:
            return f"Actif"
        else:
            return f"Ancien"

    def dehydrate_branch(self, obj):
        return f"{obj.branch.label}"

    def dehydrate_promotion(self, obj):
        return f"{obj.promotion.name}"


# Register your models here.

admin.site.register(Student)
