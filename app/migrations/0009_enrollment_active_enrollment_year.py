# Generated by Django 4.2 on 2024-03-16 23:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0008_grade_average_alter_grade_enrollment_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='enrollment',
            name='active',
            field=models.BooleanField(blank=True, default=True, null=True),
        ),
        migrations.AddField(
            model_name='enrollment',
            name='year',
            field=models.PositiveSmallIntegerField(blank=True, choices=[(1, 'Première'), (2, 'Deuxième'), (3, 'Troisième')], null=True),
        ),
    ]
