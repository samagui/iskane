from django.urls import path

from app.views import setting, students, years, school, promotion, branch, enrollment, grades

urlpatterns = [
    path('settings/', setting.settings, name='settings'),
    path('students/', students.StudentsListView.as_view(), name='student-list'),
    path('students/actives/', students.ActiveStudentsListView.as_view(), name='student-actives'),
    path('students/actives/add/', students.add_student, name='add-new-student'),
    path('students/actives/<str:id>/update/', students.update_student, name='update-student'),
    path('students/olds/', students.OldStudentsListView.as_view(), name='student-old'),
    path('students/olds/add/', students.add_old, name='add-old-student'),
    # path('students/save-new/', students.dropzone_image, name='save-new-student'),
    path('students/<str:id>/', students.detail_student, name='student-details'),

    path('years/', years.list, name='year-list'),
    path('years/create', years.create, name='create-year'),
    path('years/<str:id>/update/', years.update, name="update-year"),

    path('promotions/create', promotion.create, name='create-promo'),
    path('promotions/<str:id>/update/', promotion.update, name="update-promo"),

    path('schools/create', school.create, name='create-school'),
    path('schools/<str:id>/update/', school.update, name="update-school"),

    path('branches/create', branch.create, name='create-branch'),
    path('branches/<str:id>/update/', branch.update, name="update-branch"),

    path('enrollments/news/', enrollment.newEnrollments, name='new-enrollments'),
    path('enrollments/news/add/', enrollment.add_active, name='add-active'),
    path('enrollments/news/<str:id>/update/', enrollment.update_active, name='update-active'),
    path('enrollments/olds/', enrollment.oldEnrollments, name='old-enrollments'),
    path('enrollments/olds/add/', enrollment.add_old, name='add-old-enr'),
    path('enrollments/olds/<str:id>/update/', enrollment.update_old, name='update-old-enr'),

    path('results/news/', grades.newGrades, name='new-grades'),
    # path('grades/news/<str:id>/', enrollment.add_active_grade, name='create-active-grade'),
    path('results/olds/', grades.oldGrades, name='old-grades'),

    path('save_grade/', enrollment.save_grade, name='save_grade'),
    path('save_grade_old/', enrollment.save_grade_old, name='save_grade_old'),

]
