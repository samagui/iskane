from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from app.models import Student, Promotion, Branch, School


@login_required()
def index(request):
    active_students = Student.objects.filter(active=True).count()
    pays = Student.objects.values('country').distinct()
    branches = Branch.objects.all()
    schools = School.objects.all()
    branch_labels = []
    branch_datas = []
    branches_datas = []
    school_labels = []
    school_datas = []
    for br in branches:
        branch_labels.append(br.code)
        branch_datas.append(Student.objects.filter(branch=br).count())

    for school in schools:
        school_labels.append(school.code)
        school_datas.append(Student.objects.filter(branch__school__code=school.code).count())
        branches_datas.append(Branch.objects.filter(school=school).count())
    old_students = Student.objects.filter(active=False).count()
    all_students = Student.objects.all().count()
    promos = Promotion.objects.all().count()
    labels = []
    data_list = []
    datas = []
    for promo in Promotion.objects.all():
        labels.append(promo.name)
        data_list.append(Student.objects.filter(promotion=promo).count())
        datas.append({'value': Student.objects.filter(promotion=promo).count(), 'name': promo.name})
    print(data_list)
    context = {
        'active_students': active_students,
        'old_students': old_students,
        'all_students': all_students,
        'branch_labels': branch_labels,
        'branch_datas': branch_datas,
        'branches_datas': branches_datas,
        'school_labels': school_labels,
        'school_datas': school_datas,
        'promos': promos,
        'data_list': data_list,
        'labels': labels,
        'datas': datas
    }
    return render(request, 'home.html', context)
